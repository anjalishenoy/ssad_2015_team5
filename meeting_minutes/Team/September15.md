## Meeting On:
September 15th, 2015

## Time:
11:30 AM

## Place:
Himalaya 105

## Team Members
Anjali Shenoy, A.Ramya Keerthana, Dhruv Sapra, Kamishetty Sreeja, Saumya Rawat

## Agenda
 - To plan for the week ahead regarding the sofwares that we need to learn corresponding to our roles.
  - To discuss and prepare Software Requirements and Specifications Document.

## Discussion
- Planned the things to be done by September 20th , 2015.
- Each member of the team will learn the sofware required corresponding to their role for about 1 & 1/2 hour every day.
- Anjali Shenoy : Unity 5  
- A.Ramya Keerthana : Unity 5
- Dhruv Sapra : Unity 5, Adobe After Effects
- Sreeja Kamishetty : Unity 5, Java Script
- Saumya Rawat : Unity 5
- We distributed the section to be filled in SRS document among ourselves which is to be completed by today and to be appoved by client in the next meeting.
- Discussed about getting the lincensed Bluemix sofware(which is used as a frame work for unity).

## Action Points
 - We decided on what is to be discussed with the client in the next meeting
 - We decide to start learning the softwares required.

## Date of next meeting
To be decided after the next client meetiing
